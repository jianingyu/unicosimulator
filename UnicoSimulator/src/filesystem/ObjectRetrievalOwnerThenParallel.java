package filesystem;

import filesystem.exception.ObjectNotRetrievableException;

public class ObjectRetrievalOwnerThenParallel implements ObjectRetrievalPolicy {

	@Override
	public void retrieveObject(Node node, DirFile file)
			throws ObjectNotRetrievableException {

		node.incrementMissingObjectCounter();
		boolean retrievable = false, unavailableNode = false;

		// Check the owner(s) first.
		if (file.getOwner(node) != null) {
			for (Node n : file.getOwner(node)) {
				node.incrementMessageSendingCounter();
				if (!n.isAvailable()) {
					unavailableNode = true;
				} else if (!file.isAvailableAtNode(n)) {
					node.incrementFileNotFoundCounter();
				} else {
					node.incrementFileReceiveCounter();
					file.setAvailableAtNode(node);
					retrievable = true;
				}
			}
		}
		if (retrievable)
			return;
		else if (unavailableNode)
			node.incrementTimeoutCounter();

		unavailableNode = false;

		// Not found at owner. Check other nodes.
		for (Node n : file.getFilesystem().getNodes()) {
			if (file.getOwner(node) != null) {
				if (file.getOwner(node).contains(n) || n.equals(node))
					continue;
			}
			node.incrementMessageSendingCounter();
			if (!n.isAvailable()) {
				unavailableNode = true;
			} else if (!file.isAvailableAtNode(n)) {
				node.incrementFileNotFoundCounter();
			} else {
				node.incrementFileReceiveCounter();
				file.setAvailableAtNode(node);
				retrievable = true;
			}
		}
		if (retrievable)
			return;
		else if (unavailableNode)
			node.incrementTimeoutCounter();

		node.incrementUnretrievableFile();
		throw new ObjectNotRetrievableException();
	}

}
