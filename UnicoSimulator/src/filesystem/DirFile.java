package filesystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import filesystem.exception.ObjectNotRetrievableException;

public class DirFile {
	private int refCount; // Reference count
	private int locationBitmap; // location bitmap
	private Map<Node, ArrayList<Node>> ownership = new HashMap<Node, ArrayList<Node>>();
	private Filesystem filesystem;

	public DirFile(Filesystem filesystem, Node creatorNode) {
		this.filesystem = filesystem;
		addOwner(creatorNode, creatorNode);
		this.refCount = 1;
		this.locationBitmap = 0;
		setAvailableAtNode(creatorNode);
	}

	/*
	 * When initializing the file system, every node has every object, for
	 * objects on each node, the owner is itself
	 */
	public DirFile(Filesystem filesystem, int numNodes, int locationBitmap) {
		this.filesystem = filesystem;
		// this.ownership = null;
		this.refCount = numNodes;
		this.locationBitmap = locationBitmap;
		for (int i = 0; i < numNodes; i++) {
			ArrayList<Node> owners = new ArrayList<Node> ();
			owners.add(this.filesystem.getNode(i));
			this.ownership.put(filesystem.getNode(i), owners);
		}
	}

	public void addOwner(Node node, Node owner) {
		if (ownership.containsKey(node)) {
			ownership.get(node).add(owner);
		} else {
			ArrayList<Node> owners = new ArrayList<Node>();
			owners.add(owner);
			ownership.put(node, owners);
		}
	}

	public void setAvailableAtNode(Node node) {
		this.locationBitmap |= (1 << node.getId());
	}

	public void setUnavailableAtNode(Node node) {
		this.locationBitmap &= ~(1 << node.getId());
	}

	public int getLocationBitmap() {
		return locationBitmap;
	}

	public boolean isAvailableAtNode(Node node) {
		return ((this.locationBitmap & (1 << node.getId())) != 0);
	}

	public DirFile dirFileCopy(Node creatorNode) {
		return new DirFile(getFilesystem(), creatorNode);
	}

	public void retrieveObject(Node node) throws ObjectNotRetrievableException {

		if (node == null)
			return;
		if (isAvailableAtNode(node))
			return;

		filesystem.getObjectRetrievalPolicy().retrieveObject(node, this);
	}

	public int getRefCount() {
		return refCount;
	}

	public int incrementRefCount() {
		return ++refCount;
	}

	public int decrementRefCount() {
		return --refCount;
	}

	public ArrayList<Node> getOwner(Node node) {
		if (ownership.containsKey(node)) {
			return ownership.get(node);
		} else {
			return null;
		}
	}

	public void removeNodeFromOwner(Node node, Node owner) {
		if (ownership.containsKey(node)) {
			ArrayList<Node> owners = ownership.get(node);
			for (int i = 0; i < owners.size(); i++) {
				if (owners.get(i).equals(owner)) {
					owners.remove(i);
				}
			}
		}
	}

	public Filesystem getFilesystem() {
		return filesystem;
	}

}
