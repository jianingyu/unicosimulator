package filesystem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import filesystem.exception.ObjectNotRetrievableException;

public class Tree extends DirFile {

	private List<DirFile> children = new ArrayList<DirFile>();

	public Tree(Filesystem filesystem, Node creatorNode) {
		super(filesystem, creatorNode);
	}

	public Tree(Filesystem filesystem, Node creatorNode, List<DirFile> children) {
		super(filesystem, creatorNode);
		if (children != null)
			this.children.addAll(children);
	}

	public Tree(Filesystem filesystem, int numNodes, int locationBitmap) {
		super(filesystem, numNodes, locationBitmap);
	}

	@Override
	public DirFile dirFileCopy(Node creatorNode) {
		Tree tree = new Tree(getFilesystem(), creatorNode);
		tree.children = new ArrayList<DirFile>(children);
		return tree;
	}

	public List<DirFile> getChildren(Node node) throws ObjectNotRetrievableException {
		retrieveObject(node);
		return Collections.unmodifiableList(children);
	}

	public int addChildAtEnd(DirFile file, Node node) throws ObjectNotRetrievableException {
		retrieveObject(node);
		children.add(file);
		return children.size() - 1;
	}

	public void changeChildAtPosition(int pos, DirFile file, Node node) throws ObjectNotRetrievableException {
		retrieveObject(node);
		children.set(pos, file);
	}

}
