package filesystem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import filesystem.exception.ObjectNotRetrievableException;

public class Filesystem {
	private Node[] nodes;
	private int nFiles;
	private int dirWidth;
	private int meanDepth;
	private ObjectRetrievalPolicy objectRetrievalPolicy = new ObjectRetrievalOwnerThenParallel();
	private OperationCounter createCounter;

	public OperationCounter getCreateCounter() {
		return createCounter;
	}

	public OperationCounter getWriteCounter() {
		return writeCounter;
	}

	public OperationCounter getReadCounter() {
		return readCounter;
	}

	public OperationCounter getRenameCounter() {
		return renameCounter;
	}

	public OperationCounter getDeleteCounter() {
		return deleteCounter;
	}

	public OperationCounter getMergeCounter() {
		return mergeCounter;
	}

	private OperationCounter writeCounter;
	private OperationCounter readCounter;
	private OperationCounter renameCounter;
	private OperationCounter deleteCounter;
	private OperationCounter mergeCounter;

	public Filesystem(int numNodes, int nFiles, int dirWidth) {
		this.nodes = new Node[numNodes];
		this.nFiles = nFiles;
		this.dirWidth = dirWidth;
		this.meanDepth = (int) (Math.log10(this.nFiles) / Math
				.log10(this.dirWidth));
		this.createCounter = new OperationCounter();
		this.writeCounter = new OperationCounter();
		this.deleteCounter = new OperationCounter();
		this.readCounter = new OperationCounter();
		this.renameCounter = new OperationCounter();
		this.mergeCounter = new OperationCounter();
		initialize();
	}

	public int getMeanDepth() {
		return this.meanDepth;
	}

	public void initialize() {
		// TODO Initial bitmap needs to be revisited
		int locBitmap = (1 << nodes.length) - 1;

		Root root = new Root(this, nodes.length, locBitmap);
		for (int i = 0; i < nodes.length; i++) {
			nodes[i] = new Node(i, root, true);
		}
		populateSubdir(root, 0, 0);
	}

	private void populateSubdir(Tree parent, int serial, int depth) {
		// TODO Initial bitmap needs to be revisited
		int locBitmap = (1 << nodes.length) - 1;

		try {
			depth++;
			if (depth == meanDepth) {
				for (int i = 0; i < dirWidth; i++) {
					DirFile entry = new DirFile(this, nodes.length, locBitmap);
					parent.addChildAtEnd(entry, null);
				}
			} else {
				for (int i = 0; i < dirWidth; i++) {
					Tree entry = new Tree(this, nodes.length, locBitmap);
					parent.addChildAtEnd(entry, null);
					populateSubdir(entry, i, depth);
				}
			}
		} catch (ObjectNotRetrievableException ex) {
			// This exception should not happen here, since no real retrieval is
			// expected.
		}
	}

	public DirFile split(int[] path, Node node)
			throws ObjectNotRetrievableException {
		DirFile currentFile = node.getRoot();
		Tree prevFile = null;

		for (int depth = 0; depth <= path.length; depth++) {
			if (depth != 0)
				currentFile = prevFile.getChildren(node).get(path[depth - 1]);

			if (currentFile.getRefCount() > 1) {
				currentFile.decrementRefCount();
				if (currentFile instanceof Root) {
					Root copyOfRoot = new Root(node, true, (Root) currentFile);
					node.setRoot(copyOfRoot);
					currentFile = copyOfRoot;
				} else {
					DirFile copyOfFile = currentFile.dirFileCopy(node);
					prevFile.changeChildAtPosition(path[depth - 1], copyOfFile,
							node);// not necessary to retrieve object in this
									// function
									// since already got the file through
									// getting the
									// currentFile
					currentFile = copyOfFile;
				}
			}

			if (currentFile instanceof Tree)
				prevFile = (Tree) currentFile;
			else
				break;
		}

		return currentFile;
	}

	private DirFile merge(Node node, DirFile a, DirFile b, DirFile common)
			throws ObjectNotRetrievableException {
		DirFile mergedNode = null;

		// Both are the same: either both don't exist, or were not modified, or
		// were changed in the same way
		if (a == b)
			return a;

		// B has created/modified/deleted the file (since a different than b)
		if (a == common) {
			if (a != null)
				reduceRef(a);
			if (b != null)
				increaseRef(b);
			return b;
		}

		// A has created/modified/deleted the file
		if (b == common) {
			if (b != null)
				reduceRef(b);
			if (a != null)
				increaseRef(a);
			return a;
		}

		// A has created the file, and B has deleted it. Assume modification
		// takes precedence.
		if (a != null && b == null) {
			increaseRef(a);
			return a;
		}

		// B has created the file, and A has deleted it. Assume modification
		// takes precedence.
		if (a == null && b != null) {
			increaseRef(b);
			return b;
		}

		// At this point, both a and b have been modified. They need to be
		// compared or used for conflict resolution purposes.
		a.retrieveObject(node); // * the object should be always available;
		b.retrieveObject(node);

		if (a instanceof Root) {
			mergedNode = new Root(node, false, (Root) a, (Root) b);
		} else if (a instanceof Tree) {
			mergedNode = new Tree(this, node);
		} else {
			mergedNode = new DirFile(this, node);
		}

		mergedNode.incrementRefCount();
		a.decrementRefCount();
		b.decrementRefCount();

		if (a instanceof Tree) {

			Tree aTree = (Tree) a;
			Tree bTree = (Tree) b;
			int i;

			for (i = 0; i < aTree.getChildren(node).size()
					|| i < bTree.getChildren(node).size(); i++) {
				DirFile child = null;
				if (i >= aTree.getChildren(node).size()) {
					child = merge(node, null, bTree.getChildren(node).get(i),
							(common == null || ((Tree) common)
									.getChildren(null).size() <= i) ? null
									: ((Tree) common).getChildren(node).get(i));
				} else if (i >= bTree.getChildren(node).size()) {
					child = merge(node, aTree.getChildren(node).get(i), null,
							(common == null || ((Tree) common)
									.getChildren(null).size() <= i) ? null
									: ((Tree) common).getChildren(node).get(i));
				} else {
					child = merge(node, aTree.getChildren(node).get(i), bTree
							.getChildren(node).get(i),
							(common == null || ((Tree) common)
									.getChildren(null).size() <= i) ? null
									: ((Tree) common).getChildren(node).get(i));
				}
				((Tree) mergedNode).addChildAtEnd(child, node);// object should
																// be always
																// there
																// no need to
																// retrieve
																// object
			}
		}

		return mergedNode;
	}

	public void mergeNodes(Node node1, Node node2) {
		this.mergeCounter.incrementTotalOpCounter();
		Root root1 = node1.getRoot();
		Root root2 = node2.getRoot();

		try {
			// System.out.print(root1.commonParent(root2, node1).);
			node1.setRoot((Root) merge(node1, root1, root2,
					root1.commonParent(root2, node1)));
			node2.setRoot(node1.getRoot());
		} catch (ObjectNotRetrievableException ex) {
			// TODO Handle merge with unretrievable object
			this.mergeCounter.incrementFailedOpCounter();
		}
	}

	public void create(int[] path, Node node, boolean isDirectory) {
		try {
			this.createCounter.incrementTotalOpCounter();
			Tree parent = (Tree) this.split(path, node);
			DirFile newFile = isDirectory ? new Tree(this, node) : new DirFile(
					this, node);
			parent.addChildAtEnd(newFile, node);
		} catch (ObjectNotRetrievableException ex) {
			// TODO Handle merge with unretrievable object
			this.createCounter.incrementFailedOpCounter();
		}
	}

	public void read(int[] path, Node node) {
		try {
			this.readCounter.incrementTotalOpCounter();
			DirFile current = node.getRoot();
			int depth = 0;
			while (depth < path.length) {
				current = ((Tree) current).getChildren(node).get(path[depth]);
				depth++;
			}
			current.retrieveObject(node);
		} catch (ObjectNotRetrievableException ex) {
			// TODO Handle merge with unretrievable object
			this.readCounter.incrementFailedOpCounter();
		}
	}

	public void write(int[] path, Node node) {
		try {
			this.writeCounter.incrementTotalOpCounter();
			this.split(path, node);
		} catch (ObjectNotRetrievableException ex) {
			// TODO Handle merge with unretrievable object
			this.writeCounter.incrementFailedOpCounter();
		}
	}

	public void rename(int[] path, Node node) {
		try {
			this.renameCounter.incrementTotalOpCounter();
			int[] parentDir = Arrays.copyOfRange(path, 0, path.length - 2);
			Tree parent = (Tree) this.split(parentDir, node);
			DirFile target = parent.getChildren(node)
					.get(path[path.length - 1]);

			parent.addChildAtEnd(target, node);
			parent.changeChildAtPosition(path[path.length - 1], null, node);
		} catch (ObjectNotRetrievableException ex) {
			// TODO Handle merge with unretrievable object
			this.renameCounter.incrementFailedOpCounter();
		}

	}

	public void delete(int[] path, Node node) {
		try {
			this.deleteCounter.incrementTotalOpCounter();
			int[] parentDir = Arrays.copyOfRange(path, 0, path.length - 1);
			Tree parent = (Tree) this.split(parentDir, node);
			DirFile target = parent.getChildren(node)
					.get(path[path.length - 1]);

			reduceRef(target);
			parent.changeChildAtPosition(path[path.length - 1], null, node);
		} catch (ObjectNotRetrievableException ex) {
			// TODO Handle merge with unretrievable object
			this.deleteCounter.incrementFailedOpCounter();
		}

	}

	private void reduceRef(DirFile a) throws ObjectNotRetrievableException {
		if (a != null) {
			a.decrementRefCount();

			if (a instanceof Tree) {
				for (DirFile child : ((Tree) a).getChildren(null)) {
					reduceRef(child);
				}
			}
		}
	}

	private void increaseRef(DirFile a) throws ObjectNotRetrievableException {
		if (a != null) {
			a.incrementRefCount();

			if (a instanceof Tree) {
				for (DirFile child : ((Tree) a).getChildren(null)) {
					increaseRef(child);
				}
			}
		}
	}

	public void deleteVersion(int[] path, Node node)
			throws ObjectNotRetrievableException {
		DirFile current = node.getRoot();
		int depth = 0;
		while (depth < path.length) {
			current = ((Tree) current).getChildren(node).get(path[depth]);
			depth++;
		}
		// current.retrieveObject(node1);
		if (current.isAvailableAtNode(node)) {
			current.setUnavailableAtNode(node);
			if (current.getOwner(node).size() > 1
					&& current.getOwner(node).contains(node)) {
				current.removeNodeFromOwner(node, node);
			}
		}
	}

	public void remove(int[] path, Node node1, Node node2, boolean flag)
			throws ObjectNotRetrievableException {
		DirFile current = node1.getRoot();
		int depth = 0;
		while (depth < path.length) {
			current = ((Tree) current).getChildren(node1).get(path[depth]);
			depth++;
		}
		// current.retrieveObject(node1);
		// if flag is true, delete the current version locally moving it to node2
		if (flag) {
			if (current.isAvailableAtNode(node1)) {
				current.setUnavailableAtNode(node1);
				current.removeNodeFromOwner(node1, node1);
				current.setAvailableAtNode(node2);
				current.addOwner(node1, node2);
				current.addOwner(node2, node2);
			}
		}
		// if flag is false, copy the file to node2
		else {
			if (current.isAvailableAtNode(node1)) {
				current.setAvailableAtNode(node2);
				current.addOwner(node1, node2);
				current.addOwner(node2, node2);
				current.addOwner(node2, node1);
			}
		}
	}

	public Node[] getNodes() {
		return nodes;
	}

	public Node getNode(int i) {
		return nodes[i];
	}

	public int[] pickupFileRandom(Node node, ArrayList<Integer> path)
			throws ObjectNotRetrievableException {
		DirFile current = node.getRoot();
		ArrayList<Integer> fullPath = null;
		try {
			if (path != null) {
				fullPath = new ArrayList<Integer>(path);
				for (int i = 0; i < path.size(); i++) {
					current = ((Tree) current).getChildren(null).get(
							path.get(i));
					if (current == null) {
						System.out.print("null");
						return pickupFileRandom(node, path);
					}
				}
			} else {
				fullPath = new ArrayList<Integer>();
			}
			while (current instanceof Tree) {
				int num = randInt(0,
						((Tree) current).getChildren(null).size() - 1);
				fullPath.add(num);
				current = ((Tree) current).getChildren(null).get(num);
				if (current == null) {
					System.out.print("null");
					return pickupFileRandom(node, path);
				}
			}
			if (current == null) {
				System.out.print("null");
				return pickupFileRandom(node, path);
			}

		} catch (ObjectNotRetrievableException ex) {
			// Exception should not happen because no real retrieval is
			// happening
		}

		return buildIntArray(fullPath);
	}

	/*
	 * Randomly pick up a file that exists on the node
	 */
	public int[] pickupFileRandomExist(Node node, ArrayList<Integer> path)
			throws ObjectNotRetrievableException {
		DirFile current = node.getRoot();
		ArrayList<Integer> fullPath = null;
		try {
			if (path != null) {
				fullPath = new ArrayList<Integer>(path);
				for (int i = 0; i < path.size(); i++) {
					current = ((Tree) current).getChildren(null).get(
							path.get(i));
					if (current == null) {
						System.out.print("null");
						return pickupFileRandomExist(node, path);
					}
				}
			} else {
				fullPath = new ArrayList<Integer>();
			}
			while (current instanceof Tree) {
				int num = randInt(0,
						((Tree) current).getChildren(null).size() - 1);
				fullPath.add(num);
				current = ((Tree) current).getChildren(null).get(num);
				if (current == null) {
					System.out.print("null");
					return pickupFileRandomExist(node, path);
				}
			}
			if (current == null) {
				System.out.print("null");
				return pickupFileRandomExist(node, path);
			}
			if (!current.isAvailableAtNode(node))
				return pickupFileRandomExist(node, path);

		} catch (ObjectNotRetrievableException ex) {
			// Exception should not happen because no real retrieval is
			// happening
		}

		return buildIntArray(fullPath);
	}

	public int[] pickupTreeRandom(Node node, ArrayList<Integer> path) {
		DirFile current = node.getRoot();
		ArrayList<Integer> fullPath = null;

		// System.out.print("randomDepth = " + randomDepth + "\n");
		int depth = 0;
		try {
			if (path != null) {
				fullPath = new ArrayList<Integer>(path);
				for (int i = 0; i < path.size(); i++) {
					current = ((Tree) current).getChildren(null).get(
							path.get(i));
					depth++;
				}
			} else {
				fullPath = new ArrayList<Integer>();
			}
			int randomDepth = randInt(fullPath.size() + 1, meanDepth - 1);
			while (current instanceof Tree && depth < randomDepth) {
				int num = randInt(0,
						((Tree) current).getChildren(null).size() - 1);
				fullPath.add(num);
				current = ((Tree) current).getChildren(null).get(num);
				depth++;
			}
		} catch (ObjectNotRetrievableException ex) {
			// Exception should not happen because no real retrieval is
			// happening
		}
		if (!(current instanceof Tree)) {
			path.remove(path.size() - 1);
		}
		return buildIntArray(fullPath);

	}

	private int[] buildIntArray(ArrayList<Integer> integers) {
		int[] ints = new int[integers.size()];
		int i = 0;
		for (Integer n : integers) {
			ints[i++] = n;
		}
		return ints;
	}

	private int randInt(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt(max - min + 1) + min;
		return randomNum;
	}

	public static void main(String[] args) throws ObjectNotRetrievableException {
		// test filesystem initialization by printing all the paths and count
		// the number
		// of paths
		Filesystem filesystem = new Filesystem(4, 8, 2);
		int[] path = { 0, 0, 0 };
		// test the function of pickupFileRandom and pickupTreeRandom
		filesystem.delete(path, filesystem.getNode(0));
		DirFile current = filesystem.getNode(0).getRoot();

		for (int i = 0; i < path.length; i++) {
			current = ((Tree) current).getChildren(null).get(path[i]);
			if (current == null) {
				System.out.print("current == null" + i);
			}
		}
	}

	public ObjectRetrievalPolicy getObjectRetrievalPolicy() {
		return objectRetrievalPolicy;
	}

	public void setObjectRetrievalPolicy(
			ObjectRetrievalPolicy objectRetrievalPolicy) {
		this.objectRetrievalPolicy = objectRetrievalPolicy;
	}
}
