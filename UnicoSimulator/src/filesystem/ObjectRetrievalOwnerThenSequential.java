package filesystem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import filesystem.exception.ObjectNotRetrievableException;

public class ObjectRetrievalOwnerThenSequential implements
		ObjectRetrievalPolicy {

	@Override
	public void retrieveObject(Node node, DirFile file)
			throws ObjectNotRetrievableException {

		node.incrementMissingObjectCounter();

		// Check the owner(s) first.
		if (file.getOwner(node) != null) {
			for (Node n : file.getOwner(node)) {
				node.incrementMessageSendingCounter();
				if (!n.isAvailable()) {
					node.incrementTimeoutCounter();
				} else if (!file.isAvailableAtNode(n)) {
					node.incrementFileNotFoundCounter();
				} else {
					node.incrementFileReceiveCounter();
					file.setAvailableAtNode(node);
					return;
				}
			}
		}

		List<Node> shuffledNodes = new ArrayList<Node>(Arrays.asList(file
				.getFilesystem().getNodes()));
		if (file.getOwner(node) != null) {
			shuffledNodes.removeAll(file.getOwner(node));
		}
		shuffledNodes.remove(node);
		Collections.shuffle(shuffledNodes);

		// Not found at owner. Check other nodes.
		for (Node n : shuffledNodes) {
			node.incrementMessageSendingCounter();
			if (!n.isAvailable()) {
				node.incrementTimeoutCounter();
			} else if (!file.isAvailableAtNode(n)) {
				node.incrementFileNotFoundCounter();
			} else {
				node.incrementFileReceiveCounter();
				file.setAvailableAtNode(node);
				return;
			}
		}

		node.incrementUnretrievableFile();
		throw new ObjectNotRetrievableException();
	}

}
