package filesystem;

import filesystem.exception.ObjectNotRetrievableException;

public interface ObjectRetrievalPolicy {
	
	public void retrieveObject(Node node, DirFile file) throws ObjectNotRetrievableException;
}
