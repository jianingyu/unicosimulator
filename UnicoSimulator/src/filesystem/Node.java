package filesystem;

import filesystem.exception.ObjectNotRetrievableException;

public class Node {
	private int messageSendingCounter;
	private int missingObjectCounter;
	private int fileReceiveCounter;
	private int fileNotFoundCounter;
	private int timeoutCounter;
	private int unretrievableFileCounter;
	
	private boolean available;
//	private boolean active;
	private Root root;
	private int id;

	public Node(int id, Root root, boolean available) {
		this.id = id;
		this.root = root;
		this.setAvailable(available);
		this.missingObjectCounter = 0;
		this.messageSendingCounter = 0;	
		this.fileReceiveCounter = 0;
		this.fileNotFoundCounter = 0;
		this.timeoutCounter = 0;
		this.unretrievableFileCounter = 0;
	}
	public int getNodeId() {
		return id;
	}
	public Root getRoot() {
		return root;
	}

	public void setRoot(Root root) throws ObjectNotRetrievableException {
		root.retrieveObject(this);
		this.root = root;
	}

	public int incrementFileReceiveCounter() {
		return ++fileReceiveCounter;
	}
	public int incrementFileNotFoundCounter() {
		return ++fileNotFoundCounter;
	}
	public int incrementTimeoutCounter() {
		return ++timeoutCounter;
	}
	public int incrementMissingObjectCounter() {
		return ++missingObjectCounter;
	}
	public int incrementMessageSendingCounter() {
		return ++messageSendingCounter;
	}
	public int getId() {
		return id;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public int incrementUnretrievableFile() {
		return ++unretrievableFileCounter;
	}
	public int getFileReceiveCounter() {
		return fileReceiveCounter;
	}
	public int getFileNotFoundCounter() {
		return fileNotFoundCounter;
	}
	public int getTimeoutCounter() {
		return timeoutCounter;
	}
	public int getMissingObjectCounter() {
		return missingObjectCounter;
	}
	public int getUnretrievableFile() {
		return unretrievableFileCounter;
	}
	public int getMessageSendingCounter() {
		return messageSendingCounter;
	}
}
