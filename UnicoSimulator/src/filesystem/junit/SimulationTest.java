package filesystem.junit;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import simulation.*;
import filesystem.*;
import filesystem.exception.ObjectNotRetrievableException;

public class SimulationTest {
	EDSimulator simulator;

	public void setup(String path, String file) throws IOException,
			ObjectNotRetrievableException {
		simulator = new EDSimulator(path, file);
		simulator.parseFile();
		simulator.printResult();
	}

	@Test
	public void testRefCount1() throws IOException,
			ObjectNotRetrievableException {
		String path = "/Users/Jianing/Documents/unicosimulator/UnicoSimulator/src";
		String file = "refcounttest1.txt";
		this.setup(path, file);
		for (int i = 0; i < 100; i++) {
			int[] randompath = simulator.getFilesystem().pickupFileRandom(
					simulator.getFilesystem().getNode(3), null);
			DirFile current = simulator.getFilesystem().getNode(3).getRoot();

			for (int j = 0; j < randompath.length; j++) {
				current = ((Tree) current).getChildren(null).get(randompath[j]);
			}
			// System.out.print(Arrays.toString(randompath) + "\n");
			if (simulator.modefiedPaths
					.containsKey(Arrays.toString(randompath))) {
				assertEquals(
						simulator.getFilesystem().getNodes().length
								- simulator.modefiedPaths.get(
										Arrays.toString(randompath)).size(),
						current.getRefCount());
			} else {
				assertEquals(simulator.getFilesystem().getNodes().length,
						current.getRefCount());
			}
		}
	}

	@Test
	public void testRefCount2() throws IOException,
			ObjectNotRetrievableException {
		String path = "/Users/Jianing/Documents/unicosimulator/UnicoSimulator/src";
		String file = "refcounttest2.txt";
		this.setup(path, file);
		for (int i = 0; i < 100; i++) {
			int[] randompath = simulator.getFilesystem().pickupFileRandom(
					simulator.getFilesystem().getNode(3), null);
			DirFile current = simulator.getFilesystem().getNode(3).getRoot();

			for (int j = 0; j < randompath.length; j++) {
				current = ((Tree) current).getChildren(null).get(randompath[j]);
			}
			// System.out.print(Arrays.toString(randompath) + "\n");
			if (simulator.modefiedPaths
					.containsKey(Arrays.toString(randompath))) {
				if ((simulator.modefiedPaths.get(Arrays.toString(randompath))
						.contains(0))
						|| simulator.modefiedPaths.get(
								Arrays.toString(randompath)).contains(1)) {
					if (simulator.modefiedPaths
							.get(Arrays.toString(randompath)).contains(2))
						assertEquals(2, current.getRefCount());
					else
						assertEquals(3, current.getRefCount());
				} else {
					assertEquals(
							simulator.getFilesystem().getNodes().length
									- simulator.modefiedPaths.get(
											Arrays.toString(randompath)).size(),
							current.getRefCount());
				}
			} else {
				assertEquals(simulator.getFilesystem().getNodes().length,
						current.getRefCount());
			}
		}

		ArrayList<Integer> tempPath = new ArrayList<Integer>();
		printAllPaths(tempPath, simulator.getFilesystem().getNode(0).getRoot());
		for (int i = 0; i < 100; i++) {
			int[] randompath = simulator.getFilesystem().pickupFileRandom(
					simulator.getFilesystem().getNode(0), null);
			DirFile current = simulator.getFilesystem().getNode(0).getRoot();

			for (int j = 0; j < randompath.length; j++) {
				current = ((Tree) current).getChildren(null).get(randompath[j]);
			}

			if (simulator.modefiedPaths
					.containsKey(Arrays.toString(randompath))) {
				if ((simulator.modefiedPaths.get(Arrays.toString(randompath))
						.contains(0))
						|| simulator.modefiedPaths.get(
								Arrays.toString(randompath)).contains(1)) {
					assertEquals(2, current.getRefCount());

				}
			} else {

				if (simulator.newaddedPaths.containsKey(Arrays
						.toString(randompath))) {
					assertEquals(2, current.getRefCount());
				} else {
					assertEquals(simulator.getFilesystem().getNodes().length,
							current.getRefCount());
				}
			}
		}
	}

	@Test
	public void testRefCount3() throws IOException,
			ObjectNotRetrievableException {
		String path = "/Users/Jianing/Documents/unicosimulator/UnicoSimulator/src";
		String file = "refcounttest3.txt";
		this.setup(path, file);
		for (int i = 0; i < 100; i++) {
			int[] randompath = simulator.getFilesystem().pickupFileRandom(
					simulator.getFilesystem().getNode(3), null);
			DirFile current = simulator.getFilesystem().getNode(3).getRoot();

			for (int j = 0; j < randompath.length; j++) {
				current = ((Tree) current).getChildren(null).get(randompath[j]);
			}
			System.out.print(Arrays.toString(randompath) + "\n");
			if (simulator.modefiedPaths
					.containsKey(Arrays.toString(randompath))
					&& simulator.deletedPaths.containsKey(Arrays
							.toString(randompath))) {
				int num = simulator.modefiedPaths.get(
						Arrays.toString(randompath)).size()
						+ simulator.deletedPaths.get(
								Arrays.toString(randompath)).size();
				for (int k = 0; k < 3; k++) {
					if (simulator.modefiedPaths
							.get(Arrays.toString(randompath)).contains(k)
							&& simulator.deletedPaths.get(
									Arrays.toString(randompath)).contains(k))
						num--;
				}
				assertEquals(simulator.getFilesystem().getNodes().length - num,
						current.getRefCount());
			} else if ((!simulator.modefiedPaths.containsKey(Arrays
					.toString(randompath)))
					&& simulator.deletedPaths.containsKey(Arrays
							.toString(randompath))) {
				assertEquals(
						simulator.getFilesystem().getNodes().length
								- simulator.deletedPaths.get(
										Arrays.toString(randompath)).size(),
						current.getRefCount());
			} else if (simulator.modefiedPaths.containsKey(Arrays
					.toString(randompath))
					&& (!simulator.deletedPaths.containsKey(Arrays
							.toString(randompath)))) {
				assertEquals(
						simulator.getFilesystem().getNodes().length
								- simulator.modefiedPaths.get(
										Arrays.toString(randompath)).size(),
						current.getRefCount());
			} else {
				assertEquals(simulator.getFilesystem().getNodes().length,
						current.getRefCount());
			}
		}
	}

	@Test
	public void testMessageCounter1() throws ObjectNotRetrievableException,
			IOException {
		String path = "/Users/Jianing/Documents/unicosimulator/UnicoSimulator/src";
		String file = "messagecounttest1.txt";
		this.setup(path, file);
		PathCounter pathChanged = new PathCounter(0);
		DirFile currentA = simulator.getFilesystem().getNode(1).getRoot();
		DirFile currentB = simulator.getFilesystem().getNode(0).getRoot();
		findAllChangedTree(currentA, currentB, pathChanged);
		Filesystem filesystem = simulator.getFilesystem();
		filesystem.mergeNodes(filesystem.getNode(0), filesystem.getNode(1));
		assertEquals(simulator.getFilesystem().getNode(0)
				.getMissingObjectCounter(), pathChanged.getNumofpaths());

	}
	@Test
	public void testMessageCounter2() throws ObjectNotRetrievableException,
			IOException {
		String path = "/Users/Jianing/Documents/unicosimulator/UnicoSimulator/src";
		String file = "messagecounttest2.txt";
		this.setup(path, file);
		PathCounter pathChanged = new PathCounter(0);
		DirFile currentA = simulator.getFilesystem().getNode(1).getRoot();
		DirFile currentB = simulator.getFilesystem().getNode(0).getRoot();
		findAllChangedTree(currentA, currentB, pathChanged);
		Filesystem filesystem = simulator.getFilesystem();
		filesystem.mergeNodes(filesystem.getNode(0), filesystem.getNode(1));
		assertEquals(simulator.getFilesystem().getNode(0)
				.getMissingObjectCounter(), pathChanged.getNumofpaths());

	}
	@Test
	public void testRemove1() throws IOException, ObjectNotRetrievableException {
		String path = "/Users/Jianing/Documents/unicosimulator/UnicoSimulator/src";
		String file = "testremove1.txt";
		this.setup(path, file);
		
	}
	private static void printAllPaths(ArrayList<Integer> path, DirFile current)
			throws ObjectNotRetrievableException {
		if (!(current instanceof Tree)) {
			for (int i = 0; i < path.size(); i++) {
				System.out.print("/" + path.get(i));

			}
			System.out.print(" referenceCount = " + current.getRefCount()
					+ "\n");
		} else {
			for (int i = 0; i < ((Tree) current).getChildren(null).size(); i++) {
				path.add(i);
				DirFile nextCurrent = ((Tree) current).getChildren(null).get(i);
				printAllPaths(path, nextCurrent);
				path.remove(path.size() - 1);
			}
		}
	}

	private static void findAllChangedTree(DirFile currentA, DirFile currentB,
			PathCounter changedTree) throws ObjectNotRetrievableException {
		if (currentA == null || currentB == null)
			return;
		if (currentA instanceof Tree) {
			if (currentA.getRefCount() == 1 && currentB.getRefCount() == 1)
				changedTree.increment();
			for (int i = 0; i < ((Tree) currentA).getChildren(null).size()
					&& i < ((Tree) currentB).getChildren(null).size(); i++) {
				DirFile nextCurrentA = ((Tree) currentA).getChildren(null).get(
						i);
				DirFile nextCurrentB = ((Tree) currentB).getChildren(null).get(
						i);
				findAllChangedTree(nextCurrentA, nextCurrentB, changedTree);
			}
		} else {
			if (currentA.getRefCount() == 1 && currentB.getRefCount() == 1)
				changedTree.increment();
		}
	}

	private static class PathCounter {
		private int numofpaths = 0;

		public PathCounter(int value) {
			this.numofpaths = value;
		}

		public void increment() {
			this.numofpaths++;
		}

		public int getNumofpaths() {
			return numofpaths;
		}
	}
}
