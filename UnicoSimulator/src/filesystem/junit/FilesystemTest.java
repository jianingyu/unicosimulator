package filesystem.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import filesystem.*;
import filesystem.exception.ObjectNotRetrievableException;

public class FilesystemTest {
	Filesystem filesystem;
	private int numNodes;
	private int nFiles;
	private int dirWidth;
	private int[] path;
	private int nodeId;
	private int nodeId2;

	public void setup(int numNodes, int nFiles, int dirWidth) {
		this.numNodes = numNodes;
		this.nFiles = nFiles;
		this.dirWidth = dirWidth;
		filesystem = new Filesystem(this.numNodes, this.nFiles, this.dirWidth);
	}

	public void setup(int numNodes, int nFiles, int dirWidth, int[] path,
			int nodeId, int nodeId2) {
		setup(numNodes, nFiles, dirWidth);
		this.path = path;
		this.nodeId = nodeId;
		this.nodeId2 = nodeId2;
	}

	@Test
	public void testSplitForDifferentDirFile() {
		try {

			setup(4, 8, 2, null, 0, 2);

			Node node = filesystem.getNode(nodeId);

			int[] path = filesystem.pickupFileRandom(node, null);
			DirFile targetFile = filesystem.split(path, node);

			DirFile splittedDir = node.getRoot();
			DirFile otherDir = filesystem.getNode(nodeId2).getRoot();

			int depth = 0;

			while (depth < path.length) {
				assertNotSame(splittedDir, otherDir);
				assertEquals(1, splittedDir.getRefCount());
				assertEquals(numNodes - 1, otherDir.getRefCount());
				splittedDir = ((Tree) splittedDir).getChildren(null).get(
						path[depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(path[depth]);
				depth++;
			}
			assertTrue(splittedDir.equals(targetFile));
		} catch (ObjectNotRetrievableException ex) {
			// It is not possible to catch this exception here
			System.out.print("exception");
		}
	}

	@Test
	public void testSplitForDifferentDirFile2() {
		try {
			setup(4, 8, 2, null, 0, 2);

			Node node = filesystem.getNode(nodeId);
			int[][] paths = new int[3][];
			paths[0] = new int[] { 0 };
			paths[1] = new int[] { 0, 0 };
			paths[2] = new int[] { 0, 0, 0 };
			for (int i = 0; i < 3; i++) {
				filesystem.split(paths[i], node);
				DirFile splittedDir = node.getRoot();
				DirFile otherDir = filesystem.getNode(nodeId2).getRoot();
				assertNotSame(splittedDir, otherDir);
				assertEquals(1, splittedDir.getRefCount());
				assertEquals((numNodes - 1), otherDir.getRefCount());
				int depth = 0;
				while (depth < 3) {

					splittedDir = ((Tree) splittedDir).getChildren(null).get(
							paths[2][depth]);
					otherDir = ((Tree) otherDir).getChildren(null).get(
							paths[2][depth]);
					if (depth <= i)
						assertNotSame(splittedDir, otherDir);
					else
						assertSame(splittedDir, otherDir);
					assertEquals(depth <= i ? 1 : numNodes,
							splittedDir.getRefCount());
					assertEquals(depth <= i ? (numNodes - 1) : numNodes,
							otherDir.getRefCount());
					depth++;

				}

			}
		} catch (ObjectNotRetrievableException ex) {
			// It is not possible to catch this exception here
		}
	}

	@Test
	public void testSplitForDifferentDirFile3() {
		try {
			setup(4, 4, 2, null, 0, 1);

			Node node1 = filesystem.getNode(nodeId);
			Node node2 = filesystem.getNode(nodeId2);
			Node othernode = filesystem.getNode(numNodes - 1);
			int[][] paths = new int[2][];
			paths[0] = new int[] { 0, 0 };
			paths[1] = new int[] { 1, 1 };
			filesystem.split(paths[0], node1);
			filesystem.split(paths[0], node2);

			DirFile splittedDir1 = node1.getRoot();
			DirFile splittedDir2 = node2.getRoot();
			DirFile otherDir = othernode.getRoot();
			assertNotSame(splittedDir1, otherDir);
			assertNotSame(splittedDir2, otherDir);
			assertNotSame(splittedDir1, splittedDir2);
			assertEquals(1, splittedDir1.getRefCount());
			assertEquals(1, splittedDir2.getRefCount());
			assertEquals((numNodes - 2), otherDir.getRefCount());
			int depth = 0;
			while (depth < 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[0][depth]);
				splittedDir2 = ((Tree) splittedDir2).getChildren(null).get(
						paths[0][depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(
						paths[0][depth]);
				assertNotSame(splittedDir1, otherDir);
				assertNotSame(splittedDir2, otherDir);
				assertNotSame(splittedDir1, splittedDir2);
				assertEquals(1, splittedDir1.getRefCount());
				assertEquals(1, splittedDir2.getRefCount());
				assertEquals((numNodes - 2), otherDir.getRefCount());
				depth++;
			}
			splittedDir1 = node1.getRoot();
			splittedDir2 = node2.getRoot();
			otherDir = othernode.getRoot();
			depth = 0;
			while (depth < 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[1][depth]);
				splittedDir2 = ((Tree) splittedDir2).getChildren(null).get(
						paths[1][depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(
						paths[1][depth]);
				assertSame(splittedDir1, otherDir);
				assertSame(splittedDir2, otherDir);
				assertSame(splittedDir1, splittedDir2);
				assertEquals(numNodes, splittedDir1.getRefCount());
				assertEquals(numNodes, splittedDir2.getRefCount());
				assertEquals(numNodes, otherDir.getRefCount());
				depth++;
			}

		} catch (ObjectNotRetrievableException ex) {
			// It is not possible to catch this exception here
		}
	}

	@Test
	public void testSplitForDifferentDirFile4() {
		try {
			setup(4, 8, 2, null, 0, 1);

			Node node1 = filesystem.getNode(nodeId);
			Node node2 = filesystem.getNode(nodeId2);
			Node othernode = filesystem.getNode(numNodes - 1);
			int[][] paths = new int[3][];
			paths[0] = new int[] { 0, 0, 0 };
			paths[1] = new int[] { 1, 1, 1 };
			paths[2] = new int[] { 1, 0, 0 };
			filesystem.split(paths[0], node1);
			filesystem.split(paths[1], node2);
			for (int i = 0; i < 2; i++) {
				DirFile splittedDir1 = node1.getRoot();
				DirFile splittedDir2 = node2.getRoot();
				DirFile otherDir = othernode.getRoot();
				assertNotSame(splittedDir1, otherDir);
				assertNotSame(splittedDir2, otherDir);
				assertNotSame(splittedDir1, splittedDir2);
				assertEquals(1, splittedDir1.getRefCount());
				assertEquals(1, splittedDir2.getRefCount());
				assertEquals((numNodes - 2), otherDir.getRefCount());
				int depth = 0;

				while (depth < 2) {

					splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
							paths[i][depth]);
					splittedDir2 = ((Tree) splittedDir2).getChildren(null).get(
							paths[i][depth]);
					otherDir = ((Tree) otherDir).getChildren(null).get(
							paths[i][depth]);
					if (i == 0) {
						assertNotSame(splittedDir1, otherDir);
						assertSame(splittedDir2, otherDir);
						assertNotSame(splittedDir1, splittedDir2);
						assertEquals(1, splittedDir1.getRefCount());
						assertEquals(numNodes - 1, splittedDir2.getRefCount());
						assertEquals((numNodes - 1), otherDir.getRefCount());
					} else {
						assertSame(splittedDir1, otherDir);
						assertNotSame(splittedDir2, otherDir);
						assertNotSame(splittedDir1, splittedDir2);
						assertEquals(numNodes - 1, splittedDir1.getRefCount());
						assertEquals(1, splittedDir2.getRefCount());
						assertEquals((numNodes - 1), otherDir.getRefCount());
					}
					depth++;
				}
			}
			DirFile splittedDir1 = node1.getRoot();
			DirFile splittedDir2 = node2.getRoot();
			DirFile otherDir = othernode.getRoot();
			int depth = 0;
			while (depth < 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[2][depth]);
				splittedDir2 = ((Tree) splittedDir2).getChildren(null).get(
						paths[2][depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(
						paths[2][depth]);
				if (depth == 0) {
					assertSame(splittedDir1, otherDir);
					assertNotSame(splittedDir2, otherDir);
					assertNotSame(splittedDir1, splittedDir2);
				} else {
					assertSame(splittedDir1, otherDir);
					assertSame(splittedDir2, otherDir);
					assertSame(splittedDir1, splittedDir2);
				}
				assertEquals(depth == 0 ? numNodes - 1 : numNodes,
						splittedDir1.getRefCount());
				assertEquals(depth == 0 ? 1 : numNodes,
						splittedDir2.getRefCount());
				assertEquals(depth == 0 ? numNodes - 1 : numNodes,
						otherDir.getRefCount());
				depth++;
			}

		} catch (ObjectNotRetrievableException ex) {
			// It is not possible to catch this exception here
		}
	}
    /* Node 1 modify /0/0/0 then merge with Node 2, check every object along the path 
	* /0/0/0 and /1/1/1
	* also check the common parents
	*/
	@Test
	public void testMerge11() {
		try {
			setup(4, 8, 2, null, 0, 1);
			Node node1 = filesystem.getNode(nodeId);
			Node node2 = filesystem.getNode(nodeId2);
			Node othernode = filesystem.getNode(numNodes - 1);
			int[][] paths = new int[2][];
			paths[0] = new int[] { 0, 0, 0 };
			paths[1] = new int[] { 1, 1, 1 };

			filesystem.split(paths[0], node1);
			assertEquals(node1.getRoot().commonParent(node2.getRoot(), node1), node2.getRoot());
	
			filesystem.mergeNodes(node1, node2);

			DirFile splittedDir1 = node1.getRoot();
			DirFile splittedDir2 = node2.getRoot();
			DirFile otherDir = othernode.getRoot();
			assertNotSame(splittedDir1, otherDir);
			assertNotSame(splittedDir2, otherDir);
			assertSame(splittedDir1, splittedDir2);
			assertEquals(2, splittedDir1.getRefCount());
			assertEquals(2, splittedDir2.getRefCount());
			assertEquals((numNodes - 2), otherDir.getRefCount());
			int depth = 0;
			while (depth <= 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[0][depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(
						paths[0][depth]);
				assertNotSame(splittedDir1, otherDir);
				assertEquals(2, splittedDir1.getRefCount());
				assertEquals((numNodes - 2), otherDir.getRefCount());
				depth++;
			}
			splittedDir1 = node1.getRoot();
			otherDir = othernode.getRoot();
			depth = 0;
			while (depth < 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[1][depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(
						paths[1][depth]);
				assertSame(splittedDir1, otherDir);
				assertEquals(numNodes, splittedDir1.getRefCount());
				assertEquals(numNodes, otherDir.getRefCount());
				depth++;
			}

		} catch (ObjectNotRetrievableException ex) {
			// It is not possible to catch this exception here

		}
	}
	// the same as testMerge11, the only difference is node 2 merge with node 1
	@Test
	public void testMerge111() {
		try {
			setup(4, 8, 2, null, 0, 1);
			Node node1 = filesystem.getNode(nodeId);
			Node node2 = filesystem.getNode(nodeId2);
			Node othernode = filesystem.getNode(numNodes - 1);
			int[][] paths = new int[2][];
			paths[0] = new int[] { 0, 0, 0 };
			paths[1] = new int[] { 1, 1, 1 };

			filesystem.split(paths[0], node1);
			assertEquals(node2.getRoot().commonParent(node1.getRoot(), node2), node2.getRoot());
			filesystem.mergeNodes(node2, node1);

			DirFile splittedDir1 = node1.getRoot();
			DirFile splittedDir2 = node2.getRoot();
			DirFile otherDir = othernode.getRoot();
			assertNotSame(splittedDir1, otherDir);
			assertNotSame(splittedDir2, otherDir);
			assertSame(splittedDir1, splittedDir2);
			assertEquals(2, splittedDir1.getRefCount());
			assertEquals(2, splittedDir2.getRefCount());
			assertEquals((numNodes - 2), otherDir.getRefCount());
			int depth = 0;
			while (depth <= 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[0][depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(
						paths[0][depth]);
				assertNotSame(splittedDir1, otherDir);
				assertEquals(2, splittedDir1.getRefCount());
				assertEquals((numNodes - 2), otherDir.getRefCount());
				depth++;
			}
			splittedDir1 = node1.getRoot();
			otherDir = othernode.getRoot();
			depth = 0;
			while (depth < 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[1][depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(
						paths[1][depth]);
				assertSame(splittedDir1, otherDir);
				assertEquals(numNodes, splittedDir1.getRefCount());
				assertEquals(numNodes, otherDir.getRefCount());
				depth++;
			}

		} catch (ObjectNotRetrievableException ex) {
			// It is not possible to catch this exception here

		}
	}
	@Test
	public void testMerge12() {
		try {
			setup(4, 8, 2, null, 0, 1);
			Node node1 = filesystem.getNode(nodeId);
			Node node2 = filesystem.getNode(nodeId2);
			Node othernode = filesystem.getNode(numNodes - 1);
			int[][] paths = new int[3][];
			paths[0] = new int[] { 0, 0, 0 };
			paths[1] = new int[] { 1, 0, 0 };
			paths[2] = new int[] { 1, 1, 1 };

			filesystem.split(paths[0], node1);
			filesystem.split(paths[1], node1);
			filesystem.mergeNodes(node1, node2);

			for (int i = 0; i < 2; i++) {
				DirFile splittedDir1 = node1.getRoot();
				DirFile splittedDir2 = node2.getRoot();
				DirFile otherDir = othernode.getRoot();
				assertNotSame(splittedDir1, otherDir);
				assertNotSame(splittedDir2, otherDir);
				assertSame(splittedDir1, splittedDir2);
				assertEquals(2, splittedDir1.getRefCount());
				assertEquals(2, splittedDir2.getRefCount());
				assertEquals((numNodes - 2), otherDir.getRefCount());
				int depth = 0;
				while (depth <= 2) {

					splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
							paths[i][depth]);
					otherDir = ((Tree) otherDir).getChildren(null).get(
							paths[i][depth]);
					assertNotSame(splittedDir1, otherDir);
					assertEquals(2, splittedDir1.getRefCount());
					assertEquals((numNodes - 2), otherDir.getRefCount());
					depth++;
				}
			}
			DirFile splittedDir1 = node1.getRoot();

			DirFile otherDir = othernode.getRoot();
			splittedDir1 = node1.getRoot();
			otherDir = othernode.getRoot();
			int depth = 0;
			while (depth <= 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[2][depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(
						paths[2][depth]);
				if (depth == 0)
					assertNotSame(splittedDir1, otherDir);
				else
					assertSame(splittedDir1, otherDir);
				assertEquals(depth == 0 ? 2 : numNodes,
						splittedDir1.getRefCount());
				assertEquals(depth == 0 ? 2 : numNodes, otherDir.getRefCount());
				depth++;
			}

		} catch (ObjectNotRetrievableException ex) {
			// It is not possible to catch this exception here

		}
	}

	// node 0, node 1 modify the same file
	@Test
	public void testMerge21() {
		try {
			setup(4, 8, 2, null, 0, 1);
			Node node1 = filesystem.getNode(nodeId);
			Node node2 = filesystem.getNode(nodeId2);
			Node othernode = filesystem.getNode(numNodes - 1);
			int[][] paths = new int[2][];
			paths[0] = new int[] { 0, 0, 0 };
			paths[1] = new int[] { 1, 1, 1 };

			filesystem.split(paths[0], node1);
			filesystem.split(paths[0], node2);
			filesystem.mergeNodes(node1, node2);

			DirFile splittedDir1 = node1.getRoot();
			DirFile splittedDir2 = node2.getRoot();
			DirFile otherDir = othernode.getRoot();
			assertNotSame(splittedDir1, otherDir);
			assertNotSame(splittedDir2, otherDir);
			assertSame(splittedDir1, splittedDir2);
			assertEquals(2, splittedDir1.getRefCount());
			assertEquals(2, splittedDir2.getRefCount());
			assertEquals((numNodes - 2), otherDir.getRefCount());
			int depth = 0;
			while (depth <= 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[0][depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(
						paths[0][depth]);
				assertNotSame(splittedDir1, otherDir);
				assertEquals(2, splittedDir1.getRefCount());
				assertEquals((numNodes - 2), otherDir.getRefCount());
				depth++;
			}
			splittedDir1 = node1.getRoot();
			otherDir = othernode.getRoot();
			depth = 0;
			while (depth <= 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[1][depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(
						paths[1][depth]);

				assertSame(splittedDir1, otherDir);
				assertEquals(numNodes, splittedDir1.getRefCount());
				assertEquals(numNodes, otherDir.getRefCount());
				depth++;
			}

		} catch (ObjectNotRetrievableException ex) {
			// It is not possible to catch this exception here

		}
	}

	// node 0, node 1 modify the same file
	@Test
	public void testMerge22() {
		try {
			setup(4, 8, 2, null, 0, 1);
			Node node1 = filesystem.getNode(nodeId);
			Node node2 = filesystem.getNode(nodeId2);
			Node othernode = filesystem.getNode(numNodes - 1);
			int[][] paths = new int[2][];
			paths[0] = new int[] { 0, 0, 0 };
			paths[1] = new int[] { 1, 1, 1 };

			filesystem.split(paths[0], node1);
			filesystem.split(paths[0], node2);
			filesystem.mergeNodes(node1, node2);

			DirFile splittedDir1 = node1.getRoot();
			DirFile splittedDir2 = node2.getRoot();
			DirFile otherDir = othernode.getRoot();
			assertNotSame(splittedDir1, otherDir);
			assertNotSame(splittedDir2, otherDir);
			assertSame(splittedDir1, splittedDir2);
			assertEquals(2, splittedDir1.getRefCount());
			assertEquals(2, splittedDir2.getRefCount());
			assertEquals((numNodes - 2), otherDir.getRefCount());
			int depth = 0;
			while (depth <= 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[0][depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(
						paths[0][depth]);
				assertNotSame(splittedDir1, otherDir);
				assertEquals(2, splittedDir1.getRefCount());
				assertEquals((numNodes - 2), otherDir.getRefCount());
				depth++;
			}
			splittedDir1 = node1.getRoot();
			otherDir = othernode.getRoot();
			depth = 0;
			while (depth <= 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[1][depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(
						paths[1][depth]);

				assertSame(splittedDir1, otherDir);
				assertEquals(numNodes, splittedDir1.getRefCount());
				assertEquals(numNodes, otherDir.getRefCount());
				depth++;
			}

		} catch (ObjectNotRetrievableException ex) {
			// It is not possible to catch this exception here

		}
	}

	@Test
	public void testMerge23() {
		try {
			setup(4, 8, 2, null, 0, 1);
			Node node1 = filesystem.getNode(nodeId);
			Node node2 = filesystem.getNode(nodeId2);
			Node othernode = filesystem.getNode(numNodes - 1);
			int[][] paths = new int[3][];
			paths[0] = new int[] { 0, 0, 0 };
			paths[1] = new int[] { 1, 1, 1 };
			paths[2] = new int[] { 1, 0, 0 };
			filesystem.split(paths[0], node1);
			filesystem.split(paths[1], node2);
			filesystem.mergeNodes(node1, node2);
			for (int i = 0; i < 2; i++) {
				DirFile splittedDir1 = node1.getRoot();
				DirFile splittedDir2 = node2.getRoot();
				DirFile otherDir = othernode.getRoot();
				assertNotSame(splittedDir1, otherDir);
				assertNotSame(splittedDir2, otherDir);
				assertSame(splittedDir1, splittedDir2);
				assertEquals(2, splittedDir1.getRefCount());
				assertEquals(2, splittedDir2.getRefCount());
				assertEquals((numNodes - 2), otherDir.getRefCount());
				int depth = 0;
				while (depth <= 2) {

					splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
							paths[i][depth]);
					otherDir = ((Tree) otherDir).getChildren(null).get(
							paths[i][depth]);
					assertNotSame(splittedDir1, otherDir);
					assertEquals(2, splittedDir1.getRefCount());
					assertEquals((numNodes - 2), otherDir.getRefCount());
					depth++;
				}
			}
			DirFile splittedDir1 = node1.getRoot();
			DirFile otherDir = othernode.getRoot();
			int depth = 0;
			while (depth <= 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[2][depth]);
				otherDir = ((Tree) otherDir).getChildren(null).get(
						paths[2][depth]);
				if (depth == 0)
					assertNotSame(splittedDir1, otherDir);
				else
					assertSame(splittedDir1, otherDir);
				assertEquals(depth == 0 ? 2 : numNodes,
						splittedDir1.getRefCount());
				assertEquals(depth == 0 ? 2 : numNodes, otherDir.getRefCount());
				depth++;
			}

		} catch (ObjectNotRetrievableException ex) {
			// It is not possible to catch this exception here

		}
	}
	@Test //split after merger
	public void testMerge31() {
		try {
			setup(5, 8, 2, null, 0, 1);
			Node node1 = filesystem.getNode(nodeId);
			Node node2 = filesystem.getNode(nodeId2);
			Node othernode1 = filesystem.getNode(numNodes - 1);
			Node othernode2 = filesystem.getNode(numNodes - 2);
			int[][] paths = new int[3][];
			paths[0] = new int[] { 0, 0, 0 };
			paths[1] = new int[] { 1, 1, 1 };
			paths[2] = new int[] { 1, 0, 0 };
			filesystem.split(paths[0], node1);
			filesystem.split(paths[1], node2);
			filesystem.mergeNodes(node2, othernode1);
			filesystem.split(paths[0], node2);
			for (int i = 0; i < 2; i++) {
				DirFile splittedDir1 = node1.getRoot();
				DirFile splittedDir2 = node2.getRoot();
				DirFile otherDir1 = othernode1.getRoot();
				DirFile otherDir2 = othernode2.getRoot();
				assertNotSame(splittedDir1, otherDir1);
				assertNotSame(splittedDir2, otherDir2);
				assertNotSame(splittedDir1, splittedDir2);
				assertEquals(1, splittedDir1.getRefCount());
				assertEquals(1, splittedDir2.getRefCount());
				assertEquals(1, otherDir1.getRefCount());
				assertEquals((numNodes - 3), otherDir2.getRefCount());
				int depth = 0;
				while (depth <= 2) {

					splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
							paths[i][depth]);
					otherDir1 = ((Tree) otherDir1).getChildren(null).get(
							paths[i][depth]);
					splittedDir2 = ((Tree) splittedDir2).getChildren(null).get(
							paths[i][depth]);
					otherDir2 = ((Tree) otherDir2).getChildren(null).get(
							paths[i][depth]);
					if (i == 0) {
						assertNotSame(splittedDir1, otherDir1);
						assertNotSame(splittedDir1, splittedDir2);
						assertNotSame(splittedDir2, otherDir1);
						assertSame(otherDir1, otherDir2);
						assertEquals(1, splittedDir1.getRefCount());
						assertEquals(1, splittedDir2.getRefCount());
						assertEquals((numNodes - 2), otherDir1.getRefCount());
					}else {
						assertNotSame(splittedDir1, otherDir1);
						assertNotSame(splittedDir1, splittedDir2);
						assertSame(splittedDir2, otherDir1);
						assertSame(splittedDir1, otherDir2);
						assertEquals((numNodes - 2), splittedDir1.getRefCount());
						assertEquals(2, splittedDir2.getRefCount());
					}
					depth++;
				}
			}
			DirFile splittedDir1 = node1.getRoot();
			DirFile splittedDir2 = node2.getRoot();
			DirFile otherDir1 = othernode1.getRoot();
			DirFile otherDir2 = othernode2.getRoot();
			int depth = 0;
			while (depth <= 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[2][depth]);
				otherDir1 = ((Tree) otherDir1).getChildren(null).get(
						paths[2][depth]);
				splittedDir2 = ((Tree) splittedDir2).getChildren(null).get(
						paths[2][depth]);
				otherDir2 = ((Tree) otherDir2).getChildren(null).get(
						paths[2][depth]);
				if (depth == 0){
					assertNotSame(splittedDir1, otherDir1);
					assertNotSame(splittedDir1, splittedDir2);
					assertSame(splittedDir1, otherDir2);
					assertSame(splittedDir2, otherDir1);
					}
				else {
					assertSame(splittedDir1, otherDir1);
					assertSame(splittedDir1, otherDir2);
					assertSame(splittedDir2, otherDir1);
					}
				assertEquals(depth == 0 ? 3 : numNodes,
						splittedDir1.getRefCount());
				assertEquals(depth == 0 ? 2 : numNodes, otherDir1.getRefCount());
				depth++;
			}

		} catch (ObjectNotRetrievableException ex) {
			// It is not possible to catch this exception here

		}
	}
	@Test
	public void testMerge32() {
		try {
			setup(5, 8, 2, null, 0, 1);
			Node node1 = filesystem.getNode(nodeId);
			Node node2 = filesystem.getNode(nodeId2);
			Node othernode1 = filesystem.getNode(numNodes - 1);
			Node othernode2 = filesystem.getNode(numNodes - 2);
			int[][] paths = new int[3][];
			paths[0] = new int[] { 0, 0, 0 };
			paths[1] = new int[] { 1, 1, 1 };
			paths[2] = new int[] { 1, 0, 0 };
			filesystem.split(paths[0], node1);
			filesystem.split(paths[1], node2);
			filesystem.mergeNodes(node2, othernode1);
			filesystem.split(paths[0], node2);
			filesystem.mergeNodes(node1, node2);
			for (int i = 0; i < 2; i++) {
				DirFile splittedDir1 = node1.getRoot();
				DirFile splittedDir2 = node2.getRoot();
				DirFile otherDir1 = othernode1.getRoot();
				DirFile otherDir2 = othernode2.getRoot();
				assertNotSame(splittedDir1, otherDir1);
				assertNotSame(splittedDir2, otherDir2);
				assertSame(splittedDir1, splittedDir2);
				assertEquals(2, splittedDir1.getRefCount());		
				assertEquals(1, otherDir1.getRefCount());
				assertEquals(2, otherDir2.getRefCount());

				int depth = 0;
				while (depth <= 2) {

					splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
							paths[i][depth]);
					otherDir1 = ((Tree) otherDir1).getChildren(null).get(
							paths[i][depth]);
					splittedDir2 = ((Tree) splittedDir2).getChildren(null).get(
							paths[i][depth]);
					otherDir2 = ((Tree) otherDir2).getChildren(null).get(
							paths[i][depth]);
					if (i == 0) {
						assertNotSame(splittedDir1, otherDir1);
						assertSame(splittedDir1, splittedDir2);
						assertSame(otherDir1, otherDir2);
						assertEquals(2, splittedDir1.getRefCount());
						assertEquals((numNodes - 2), otherDir1.getRefCount());
					}else {
						assertNotSame(splittedDir1, otherDir1);
						assertSame(splittedDir1, splittedDir2);
						assertNotSame(splittedDir2, otherDir2);
						assertNotSame(otherDir1, otherDir2);
						assertEquals(2, splittedDir1.getRefCount());
						assertEquals(2, otherDir2.getRefCount());
						assertEquals(1, otherDir1.getRefCount());
					
					}
					depth++;
				}
			}
			DirFile splittedDir1 = node1.getRoot();
			DirFile splittedDir2 = node2.getRoot();
			DirFile otherDir1 = othernode1.getRoot();
			DirFile otherDir2 = othernode2.getRoot();
			int depth = 0;
			while (depth <= 2) {

				splittedDir1 = ((Tree) splittedDir1).getChildren(null).get(
						paths[2][depth]);
				otherDir1 = ((Tree) otherDir1).getChildren(null).get(
						paths[2][depth]);
				splittedDir2 = ((Tree) splittedDir2).getChildren(null).get(
						paths[2][depth]);
				otherDir2 = ((Tree) otherDir2).getChildren(null).get(
						paths[2][depth]);
				if (depth == 0){
					assertNotSame(splittedDir1, otherDir1);
					assertSame(splittedDir1, splittedDir2);
					assertNotSame(otherDir1, otherDir2);
					}
				else {
					assertSame(splittedDir1, otherDir1);
					assertSame(splittedDir1, otherDir2);
					assertSame(splittedDir2, otherDir1);
					}
				assertEquals(depth == 0 ? 2 : numNodes,
						splittedDir1.getRefCount());
				assertEquals(depth == 0 ? 1 : numNodes, otherDir1.getRefCount());
				depth++;
			}
			for(int i = 0; i < numNodes; i++) {
				System.out.print(filesystem.getNode(i).getMissingObjectCounter());
			}
		} catch (ObjectNotRetrievableException ex) {
			// It is not possible to catch this exception here

		}
	}
/*	
	private static void printAllPaths(ArrayList<Integer> path, DirFile current,
			PathCounter numOfPaths) throws ObjectNotRetrievableException {
		if (!(current instanceof Tree)) {
			for (int i = 0; i < path.size(); i++) {
				System.out.print("/" + path.get(i));

			}
			numOfPaths.increment();
			System.out.print("\n");
		} else {
			for (int i = 0; i < ((Tree) current).getChildren(null).size(); i++) {
				path.add(i);
				DirFile nextCurrent = ((Tree) current).getChildren(null).get(i);
				printAllPaths(path, nextCurrent, numOfPaths);
				path.remove(path.size() - 1);
			}
		}
	} 

	private static class PathCounter {
		private int numofpaths = 0;

		public PathCounter(int value) {
			this.numofpaths = value;
		}

		public void increment() {
			this.numofpaths++;
		}

		public int getNumofpaths() {
			return numofpaths;
		}
	}*/

}
