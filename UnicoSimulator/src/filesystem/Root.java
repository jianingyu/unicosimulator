package filesystem;

import java.util.Arrays;
import java.util.List;

import filesystem.exception.ObjectNotRetrievableException;

public class Root extends Tree {
	private List<Root> parents;

	public Root(Filesystem filesystem, int numNodes, int locationBitmap) {
		super(filesystem, numNodes, locationBitmap);
	}

	public Root(Node creatorNode, boolean copyChildren, Root... parents) throws ObjectNotRetrievableException {
		super(parents[0].getFilesystem(), creatorNode, copyChildren ? parents[0]
				.getChildren(creatorNode) : null);
		this.parents = Arrays.asList(parents);
	}

	public Root commonParent(Root other, Node node) throws ObjectNotRetrievableException {

		this.retrieveObject(node);
		other.retrieveObject(node);
		if (this.parents != null && this.parents.contains(other))
			return other;
		if (other.parents != null && other.parents.contains(this))
			return this;
		if (this.parents.size() == 1 && this.parents.equals(other.parents))
			return parents.get(0);
		else
			return null;
	}
}
