package filesystem;

public class OperationCounter {
	
	private int totalOpCounter;
	private int failedOpCounter;
	public OperationCounter() {
		this.totalOpCounter = 0;
		this.failedOpCounter = 0;
	}
	public int getTotalOpCounter() {
		return this.totalOpCounter;
	}
	public int getFailedOpCounter() {
		return this.failedOpCounter;
	}
	public void incrementTotalOpCounter() {
		++this.totalOpCounter;
	}
	public void incrementFailedOpCounter() {
		++this.failedOpCounter;
	}
}
