package filesystem.exception;

public class ObjectNotRetrievableException extends Exception {

	public ObjectNotRetrievableException() {
	}

	public ObjectNotRetrievableException(String arg0) {
		super(arg0);
	}

	public ObjectNotRetrievableException(Throwable arg0) {
		super(arg0);
	}

	public ObjectNotRetrievableException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
}
