numnodes 6
dirwidth 10
numfiles 1000
endtime 100
policy objectretrieval ownerthensequential
activenode nodeid = 0 start = 0 end = 50
{
	write prob = 100 path = /0
}
activenode nodeid = 1 start = 10 end = 60
{
	write prob = 100 path = /1
}
activenode nodeid = 2 start = 30 end = 70
{
	write prob = 100 path = /2
}
activenode nodeid = 3 start = 30 end = 70
{
	write prob = 100 path = /3
}
activenode nodeid = 4 start = 30 end = 70
{
	write prob = 100 path = /2
}
sync nodeid = 5 start = 80 end = 90 interval = 20 syncwith = all
activenode nodeid = 5 start = 95 end = 145
{
	read prob = 25 path = /0
	read prob = 25 path = /1
	read prob = 25 path = /2
	read prob = 25 path = /3
}

