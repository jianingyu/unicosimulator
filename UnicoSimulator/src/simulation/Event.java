package simulation;

public class Event implements Comparable<Event>{
	private int time;
	private long timestamp;
	private Operation operation;
	public Event(int time, long timestamp, Operation operation) {
		this.time = time;
		this.timestamp = timestamp;
		
		this.operation = operation;
	}
	public Operation getOperation() {
		return operation;
	}
	
	public int getTime() {
		return time;
	}
	public long getTimestamp() {
		return timestamp;
	}
	
	public int compareTo(Event ev) {
		if(this.time != ev.time) {
			return (this.time - ev.time);
				
		} else {
			if ( this.timestamp < ev.timestamp)
				return -1;
			else return 0;
		}	
	}	
}
