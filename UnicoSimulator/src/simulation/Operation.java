package simulation;
import java.util.ArrayList;

import filesystem.*;

public class Operation {
	public enum OperationType {
		CREATE, READ, WRITE, RENAME, DELETE, MERGENODES, SETAVAILABLE, DELETEVERSION, REMOVE
	}
	private OperationType type;
	private Node node; // node which execute this operation
	private Node secondNode; // the other node which need to transfer information to node when merge
	private ArrayList<Integer> path;
	private boolean availability;
	private boolean idleness;
	private boolean file;
	private boolean flag;
	public Operation(OperationType type, Node node) {
		this.node = node;
		this.type = type;
	}
	public Operation(OperationType type, Node node, ArrayList<Integer> path) {
		this.node = node;
		this.type = type;
		this.setPath(path);
	}
	public Operation(OperationType type, Node node, Node secondNode, ArrayList<Integer> path, boolean flag) {
		this.node = node;
		this.secondNode = secondNode;
		this.type = type;
		this.flag = flag;
		this.setPath(path);
	}
	public Operation(OperationType type, Node node, Node secondNode) {
		this.node = node;
		this.secondNode = secondNode;
		this.type = type;
	}
	public Operation(OperationType type, Node node, boolean availability) {
		this.node = node;
		this.type = type;
		if (type == OperationType.SETAVAILABLE)
			this.availability = availability;
		
	}
	
	public OperationType getType() {
		return type;
	}
	
	public Node getNode() {
		return node;
	}
	
	public Node getSecondNode() {
		return secondNode;
	}
	
	public boolean isAvailability() {
		return availability;
	}
	
	public boolean isIdleness() {
		return idleness;
	}
	public ArrayList<Integer> getPath() {
		return path;
	}
	public void setPath(ArrayList<Integer> path) {
		this.path = path;
	}
	public boolean isfile() {
		return file;
	}
	public boolean getFlag() {
		return flag;
	}
	
}
