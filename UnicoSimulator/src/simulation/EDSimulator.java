package simulation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.TreeMap;

import simulation.Operation.OperationType;
import filesystem.DirFile;
import filesystem.Filesystem;
import filesystem.Node;
import filesystem.ObjectRetrievalOwnerThenParallel;
import filesystem.ObjectRetrievalOwnerThenSequential;
import filesystem.Tree;
import filesystem.exception.ObjectNotRetrievableException;

public class EDSimulator {

	private PriorityQueue<Event> queue = new PriorityQueue<Event>(100);
	private Filesystem filesystem;
	private int numNodes;
	private int dirWidth;
	private int numFiles;
	private int endTime;
	private long timestamp = 0;
	private static int INTERVAL_DEFAULT = 20;
	private String path;
	private String workloadFile;
	public HashMap<String, HashSet<Integer>> modefiedPaths;
	public HashMap<String, HashSet<Integer>> newaddedPaths;
	public HashMap<String, HashSet<Integer>> deletedPaths;
	private BufferedWriter fop;

	public EDSimulator(String path, String workloadFile) {
		this.path = path;
		this.workloadFile = workloadFile;
		modefiedPaths = new HashMap<String, HashSet<Integer>>();
		newaddedPaths = new HashMap<String, HashSet<Integer>>();
		deletedPaths = new HashMap<String, HashSet<Integer>>();
	}

	public void parseFile() throws IOException, ObjectNotRetrievableException {
		String fileName = new String(path + "/" + workloadFile);
		BufferedReader reader = new BufferedReader(new FileReader(new File(
				fileName)));
		String line;

		while ((line = reader.readLine()) != null) {
			if (line.length() == 0)
				continue;
			if (line.substring(0, 2) == "//")
				continue;

			String[] strs = line.trim().split(" ");
			String action = strs[0];
			if (action.equalsIgnoreCase("numnodes")) {
				numNodes = Integer.parseInt(strs[1]);
			} else if (action.equalsIgnoreCase("dirwidth")) {
				dirWidth = Integer.parseInt(strs[1]);
			} else if (action.equalsIgnoreCase("numfiles")) {
				numFiles = Integer.parseInt(strs[1]);
				filesystem = new Filesystem(numNodes, numFiles, dirWidth);
			} else if (action.equalsIgnoreCase("endtime")) {
				endTime = Integer.parseInt(strs[1]);
			} else if (action.equalsIgnoreCase("policy")) {

				if (strs[1].equalsIgnoreCase("objectretrieval")) {

					if (strs[2].equalsIgnoreCase("ownerthenparallel"))
						filesystem
								.setObjectRetrievalPolicy(new ObjectRetrievalOwnerThenParallel());
					else if (strs[2].equalsIgnoreCase("ownerthensequential"))
						filesystem
								.setObjectRetrievalPolicy(new ObjectRetrievalOwnerThenSequential());
				}
				// Here come other policies later

			} else if (action.equalsIgnoreCase("activenode")) {

				Map<String, String> params = extractParameters(strs);

				int nodeId = params.containsKey("nodeid") ? Integer
						.parseInt(params.get("nodeid")) : 0;
				int startPoint = params.containsKey("start") ? Integer
						.parseInt(params.get("start")) : 0;
				int endPoint = params.containsKey("end") ? Integer
						.parseInt(params.get("end")) : endTime;

				ArrayList<OperationType> operations = new ArrayList<OperationType>();
				ArrayList<Integer> probability = new ArrayList<Integer>();
				ArrayList<Integer> secondNode = new ArrayList<Integer>();
				ArrayList<Boolean> flags = new ArrayList<Boolean>();
				ArrayList<ArrayList<Integer>> paths = new ArrayList<ArrayList<Integer>>();
				int prob = 0;

				while ((line = reader.readLine()) != null) {

					String[] innerstrs = line.trim().split(" ");
					String inneraction = innerstrs[0];
					if (inneraction.equals("{"))
						continue;
					if (inneraction.equals("}"))
						break;
					if (inneraction.equalsIgnoreCase("write")) {
						operations.add(OperationType.WRITE);
					} else if (inneraction.equalsIgnoreCase("read")) {
						operations.add(OperationType.READ);
					} else if (inneraction.equalsIgnoreCase("create")) {
						operations.add(OperationType.CREATE);
					} else if (inneraction.equalsIgnoreCase("rename")) {
						operations.add(OperationType.RENAME);
					} else if (inneraction.equalsIgnoreCase("delete")) {
						operations.add(OperationType.DELETE);
					} else if (inneraction.equalsIgnoreCase("deleteversion")) {
						operations.add(OperationType.DELETEVERSION);
					} else if (inneraction.equalsIgnoreCase("remove")) {
						operations.add(OperationType.REMOVE);
					}
					Map<String, String> innerparams = extractParameters(innerstrs);
					prob += innerparams.containsKey("prob") ? Integer
							.parseInt(innerparams.get("prob")) : 0;
					probability.add(prob);

					if (innerparams.containsKey("path")) {
						String path = innerparams.get("path");
						String[] pathArray = path.trim().split("/");
						ArrayList<Integer> pathTemp = new ArrayList<Integer>();
						for (int i = 0; i < pathArray.length - 1; i++) {
							pathTemp.add(Integer.parseInt(pathArray[i + 1]));
						}
						paths.add(pathTemp);
					} else {
						paths.add(null);
					}
					if (innerparams.containsKey("moveto")) {
						int node2Id = Integer.parseInt(innerparams
								.get("moveto"));
						secondNode.add(node2Id);
						String flag = innerparams.get("flag");
						if (flag.equalsIgnoreCase("true")) {
							flags.add(true);
						} else if (flag.equalsIgnoreCase("false")) {
							flags.add(false);
						}
					} else {
						secondNode.add(-1);
						flags.add(null);
					}

				}
				operationGenerator(nodeId, startPoint, endPoint, operations,
						probability, paths, secondNode, flags);
			} else if (action.equalsIgnoreCase("nodeunavailable")) {
				Map<String, String> params = extractParameters(strs);
				int nodeId = params.containsKey("nodeid") ? Integer
						.parseInt(params.get("nodeid")) : 0;
				int startPoint = params.containsKey("start") ? Integer
						.parseInt(params.get("start")) : 0;
				int endPoint = params.containsKey("end") ? Integer
						.parseInt(params.get("end")) : endTime;

				Operation operation = new Operation(OperationType.SETAVAILABLE,
						filesystem.getNode(nodeId), false);
				add(startPoint, filesystem.getNode(nodeId), operation);
				operation = new Operation(OperationType.SETAVAILABLE,
						filesystem.getNode(nodeId), true);
				add(endPoint, filesystem.getNode(nodeId), operation);
			} else if (action.equalsIgnoreCase("sync")) {
				Map<String, String> params = extractParameters(strs);
				int nodeId = params.containsKey("nodeid") ? Integer
						.parseInt(params.get("nodeid")) : 0;
				int startPoint = params.containsKey("start") ? Integer
						.parseInt(params.get("start")) : 0;
				int endPoint = params.containsKey("end") ? Integer
						.parseInt(params.get("end")) : endTime;
				int interval = params.containsKey("interval") ? Integer
						.parseInt(params.get("interval")) : INTERVAL_DEFAULT;
				String withWho = params.containsKey("syncwith") ? params
						.get("syncwith") : "any";

				syncGenerator(nodeId, startPoint, endPoint, interval, withWho);
			}
		}
		reader.close();
	}

	/**
	 * @param strs
	 */
	private Map<String, String> extractParameters(String[] strs) {
		Map<String, String> ret = new TreeMap<String, String>(
				String.CASE_INSENSITIVE_ORDER);

		for (int i = 1; i < strs.length; i++) {
			String name;
			String value;
			if (strs[i].endsWith("=")) { // name= value
				name = strs[i].split("=", 2)[0];
				value = i < strs.length - 1 ? strs[++i] : "";
			} else if (strs[i].contains("=")) { // name=value
				name = strs[i].split("=", 2)[0];
				value = strs[i].split("=", 2)[1];
			} else if (i < strs.length - 1 && strs[i + 1].equals("=")) { // name
																			// =
																			// value
				name = strs[i];
				++i;
				value = i < strs.length - 1 ? strs[++i] : "";
			} else if (i < strs.length - 1 && strs[i + 1].startsWith("=")) { // name
																				// =value
				name = strs[i];
				value = strs[++i].substring(1);
			} else {
				// There is no equals sign.
				continue;
			}
			ret.put(name, value);
		}
		return ret;
	}

	private void operationGenerator(int nodeId, int startPoint, int endPoint,
			ArrayList<OperationType> operations,
			ArrayList<Integer> probability, ArrayList<ArrayList<Integer>> paths,
			ArrayList<Integer> secondNode, ArrayList<Boolean> flags)
			throws ObjectNotRetrievableException {
		for (int i = startPoint; i <= endPoint; i++) {
			Node node = filesystem.getNode(nodeId);
			int randnum = randInt(0, 100);
			OperationType operationType = null;
			int k;
			for (k = 0; k < probability.size(); k++) {
				if (randnum <= probability.get(k)) {
					operationType = operations.get(k);
					break;
				}
			}
			ArrayList<Integer> path = paths.get(k);
			
			Operation operation = null;
			if(operationType == OperationType.REMOVE) {
				Node node2 = filesystem.getNode(secondNode.get(k));
				boolean flag = flags.get(k);
				operation = new Operation(operationType, node, node2, path, flag);
			} else {
				operation = new Operation(operationType, node, path);
			}
			add(i, node, operation);
		}

	}

	private void syncGenerator(int nodeId, int start, int end, int interval,
			String withWho) {
		System.out.print(withWho);
		for (int i = start; i <= end; i = i + interval) {
			if (withWho.equals("any")) {
				int randomNode = randInt(0, numNodes - 1);
				Operation operation = new Operation(OperationType.MERGENODES,
						filesystem.getNode(nodeId),
						filesystem.getNode(randomNode));
				add(i, filesystem.getNode(nodeId), operation);
			} else if (withWho.equals("all")) {
				for (int j = 0; j < numNodes; j++) {
					if (j != nodeId) {
						Operation operation = new Operation(
								OperationType.MERGENODES,
								filesystem.getNode(nodeId),
								filesystem.getNode(j));
						add(i, filesystem.getNode(nodeId), operation);
					}
				}
			} else {
				int nodeChosen = Integer.parseInt(withWho);
				Operation operation = new Operation(OperationType.MERGENODES,
						filesystem.getNode(nodeId),
						filesystem.getNode(nodeChosen));
				add(i, filesystem.getNode(nodeId), operation);
			}

		}
	}

	private int randInt(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt(max - min + 1) + min;
		return randomNum;
	}

	public void add(Event event) {
		queue.add(event);
		timestamp++;
	}

	public void add(int time, Node node, Operation operation) {
		Event event = new Event(time, timestamp, operation);
		queue.add(event);
		timestamp++;
	}

	public boolean executeNext() throws ObjectNotRetrievableException,
			IOException {
		if (queue.isEmpty()) {
			System.err.println("queue is empty, quitting" + " at time ");
			return true;
		}

		Event event = queue.remove();
		Operation operation = event.getOperation();
		Node node = operation.getNode();

		int[] path = null;
		String content = "";
		switch (operation.getType()) {
		case CREATE:
			path = filesystem.pickupTreeRandom(node, operation.getPath());
			DirFile current = node.getRoot();
			for (int i = 0; i < path.length; i++) {
				current = ((Tree) current).getChildren(null).get(path[i]);
			}
			int[] filePath;
			filePath = Arrays.copyOf(path, path.length + 1);
			Arrays.copyOfRange(path, 0, path.length);
			filePath[path.length] = ((Tree) current).getChildren(null).size();
			filesystem.create(path, node, false);
			content = new String("Nodeid = " + node.getId() + " Type = "
					+ operation.getType() + " path = "
					+ Arrays.toString(filePath) + "\n");
			if (newaddedPaths.containsKey(Arrays.toString(filePath))) {
				newaddedPaths.get(Arrays.toString(filePath)).add(node.getId());
			} else {
				HashSet<Integer> temp = new HashSet<Integer>();
				temp.add(node.getId());
				newaddedPaths.put(Arrays.toString(filePath), temp);
			}
			break;
		case READ:
			path = filesystem.pickupFileRandom(node, operation.getPath());
			filesystem.read(path, node);
			content = new String("Nodeid = " + node.getId() + " Type = "
					+ operation.getType() + " path = " + Arrays.toString(path)
					+ "\n");
			break;
		case WRITE:
			path = filesystem.pickupFileRandom(node, operation.getPath());
			if (modefiedPaths.containsKey(Arrays.toString(path))) {
				modefiedPaths.get(Arrays.toString(path)).add(node.getId());
			} else {
				HashSet<Integer> temp = new HashSet<Integer>();
				temp.add(node.getId());
				modefiedPaths.put(Arrays.toString(path), temp);
			}
			content = new String("Nodeid = " + node.getId() + " Type = "
					+ operation.getType() + " path = " + Arrays.toString(path)
					+ "\n");
			filesystem.write(path, node);
			break;
		case RENAME:
			path = filesystem.pickupFileRandom(node, operation.getPath());
			filesystem.rename(path, node);
			content = new String("Nodeid = " + node.getId() + " Type = "
					+ operation.getType() + " path = " + Arrays.toString(path)
					+ "\n");
			break;
		case DELETE:
			path = filesystem.pickupFileRandom(node, operation.getPath());
			if (deletedPaths.containsKey(Arrays.toString(path))) {
				deletedPaths.get(Arrays.toString(path)).add(node.getId());
			} else {
				HashSet<Integer> temp = new HashSet<Integer>();
				temp.add(node.getId());
				deletedPaths.put(Arrays.toString(path), temp);
			}
			filesystem.delete(path, node);
			content = new String("Nodeid = " + node.getId() + " Type = "
					+ operation.getType() + " path = " + Arrays.toString(path)
					+ "\n");
			break;
		case MERGENODES:
			if (!node.isAvailable())
				return false;

			Node node2 = operation.getSecondNode();

			content = new String("Nodeid = " + node.getId() + " Type = "
					+ operation.getType() + " Nodeid = " + node2.getId() + "\n");
			System.out.print(content);
			filesystem.mergeNodes(node, node2);
			break;
		case SETAVAILABLE:
			node.setAvailable(operation.isAvailability());
			content = new String("Nodeid = " + node.getId()
					+ " availability = " + operation.isAvailability() + "\n");
			break;
		case DELETEVERSION:
			path = filesystem.pickupFileRandomExist(node, operation.getPath());
			filesystem.deleteVersion(path, node);
			content = new String("Nodeid = " + node.getId() + " Type = "
					+ operation.getType() + " path = " + Arrays.toString(path)
					+ "\n");
			break;
		case REMOVE:
			path = filesystem.pickupFileRandomExist(node, operation.getPath());
			filesystem.remove(path, node, operation.getSecondNode(),
					operation.getFlag());
			content = new String("Nodeid = " + node.getId() + " Type = "
					+ operation.getType() + " path = " + Arrays.toString(path)
					+ " to Nodeid = " + operation.getSecondNode().getId() + "\n");
			break;

		}
		fop.write(content);
		fop.flush();
		return false;
	}

	public PriorityQueue<Event> getEvent() {
		return queue;
	}

	public Filesystem getFilesystem() {
		return filesystem;
	}

	public void printResult() throws IOException {
		try {
			modefiedPaths = new HashMap<String, HashSet<Integer>>();
			newaddedPaths = new HashMap<String, HashSet<Integer>>();
			int index = workloadFile.lastIndexOf(".");
			String fileName = new String(workloadFile.substring(0, index)
					+ "result" + workloadFile.substring(index));
			File file = new File(fileName);
			if (!file.exists()) {
				file.createNewFile();
			}
			fop = new BufferedWriter(new FileWriter(file));

			while (!executeNext()) {
			}
			fop.newLine();
			String content = new String("Nodeid\t" + "MessageSend\t"
					+ "MissingObject\t" + "ReceiveObject\t"
					+ "FileNotFoundObject\t" + "TimeOutObject\t"
					+ "UnretrievableObject\n");
			fop.write(content);
			int sumMessageSend = 0;
			int sumMiss = 0;
			int sumReceive = 0;
			int sumFileNotFound = 0;
			int sumTimeOut = 0;
			int sumUnretrievable = 0;
			for (int i = 0; i < filesystem.getNodes().length; i++) {
				content = new String(Integer.toString(i)
						+ "\t"
						+ Integer.toString(filesystem.getNode(i)
								.getMessageSendingCounter())
						+ "\t"
						+ Integer.toString(filesystem.getNode(i)
								.getMissingObjectCounter())
						+ "\t"
						+ Integer.toString(filesystem.getNode(i)
								.getFileReceiveCounter())
						+ "\t"
						+ Integer.toString(filesystem.getNode(i)
								.getFileNotFoundCounter())
						+ "\t"
						+ Integer.toString(filesystem.getNode(i)
								.getTimeoutCounter())
						+ "\t"
						+ Integer.toString(filesystem.getNode(i)
								.getUnretrievableFile()) + "\n");
				fop.write(content);
				sumMessageSend += filesystem.getNode(i)
						.getMessageSendingCounter();
				sumMiss += filesystem.getNode(i).getMissingObjectCounter();
				sumReceive += filesystem.getNode(i).getFileReceiveCounter();
				sumFileNotFound += filesystem.getNode(i)
						.getFileNotFoundCounter();
				sumTimeOut += filesystem.getNode(i).getTimeoutCounter();
				sumUnretrievable += filesystem.getNode(i)
						.getUnretrievableFile();
			}
			content = new String("sum\t" + Integer.toString(sumMessageSend)
					+ "\t" + Integer.toString(sumMiss) + "\t"
					+ Integer.toString(sumReceive) + "\t"
					+ Integer.toString(sumFileNotFound) + "\t"
					+ Integer.toString(sumTimeOut) + "\t"
					+ Integer.toString(sumUnretrievable) + "\n");
			fop.write(content);
			fop.newLine();
			content = new String(
					"\twrite\tread\tcreate\trename\tdelete\tmerge\n");
			fop.write(content);
			content = new String("failed\t"
					+ Integer.toString(filesystem.getWriteCounter()
							.getFailedOpCounter())
					+ "\t"
					+ Integer.toString(filesystem.getReadCounter()
							.getFailedOpCounter())
					+ "\t"
					+ Integer.toString(filesystem.getCreateCounter()
							.getFailedOpCounter())
					+ "\t"
					+ Integer.toString(filesystem.getRenameCounter()
							.getFailedOpCounter())
					+ "\t"
					+ Integer.toString(filesystem.getDeleteCounter()
							.getFailedOpCounter())
					+ "\t"
					+ Integer.toString(filesystem.getMergeCounter()
							.getFailedOpCounter()) + "\n");
			fop.write(content);
			content = new String("total\t"
					+ Integer.toString(filesystem.getWriteCounter()
							.getTotalOpCounter())
					+ "\t"
					+ Integer.toString(filesystem.getReadCounter()
							.getTotalOpCounter())
					+ "\t"
					+ Integer.toString(filesystem.getCreateCounter()
							.getTotalOpCounter())
					+ "\t"
					+ Integer.toString(filesystem.getRenameCounter()
							.getTotalOpCounter())
					+ "\t"
					+ Integer.toString(filesystem.getDeleteCounter()
							.getTotalOpCounter())
					+ "\t"
					+ Integer.toString(filesystem.getMergeCounter()
							.getTotalOpCounter()) + "\n");
			fop.write(content);
			fop.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String args[]) throws IOException,
			ObjectNotRetrievableException {
		EDSimulator simulator = new EDSimulator(
				"/Users/Jianing/Documents/unicosimulator/UnicoSimulator/src",
				"testremoveparallel.txt");
		simulator.parseFile();
		simulator.printResult();

	}
}
